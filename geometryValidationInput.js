/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let total = 0;

// Function to calculate beam volume
function beam(length, breadth, thickness) {
  return length * breadth * thickness;
}

function cube(side) {
  return side ** 3;
}

// input beam
function inputbeam() {
  rl.question("Length: ", function (length) {
    console.log(length);
    rl.question("Breadth: ", (breadth) => {
      console.log(breadth);
      rl.question("Thickness: ", (thickness) => {
        console.log(thickness);
        if (!isNaN(length) && !isNaN(breadth) && !isNaN(thickness)) {
          console.log(`\Volume of Beam: ${beam(length, breadth, thickness)}`);
          inputLength();
        } else {
          console.log(`length, Breadth, and Thickness must be a number\n`);
          inputbeam();
        }
      });
    });
  });
}

// input cube
function inputLength() {
  rl.question(`Side: `, (sideloc) => {
    console.log(sideloc);
    if (!isNaN(sideloc)) {
      console.log(`\Volume of Cube: ${cube(sideloc)}`);
      rl.close();
    } else {
      console.log(`Side must be a number\n`);
      inputLength();
    }
  });
}

/* End Alternative Way */

console.log(`Rectangle`);
console.log(`=========`);

inputbeam();
